const inputBil = () => {
    const x = document.getElementById("jumlahBil").value;
    const mulai = document.getElementById("start");

    
    for (var i=1; i<=x; i++){
        mulai.innerHTML += '<div class="form-group">';
        mulai.innerHTML += '<label for="angka'+i+'">Masukkan angka ke-'+i+'</label>';
        mulai.innerHTML += '<input type="text" class="form-control" id="angka'+i+'"></div>';
    }

    mulai.innerHTML += '<br><div>';
    mulai.innerHTML += '<button type="button" class="btn btn-primary" onclick="showResult()">Lihat Hasil</button></div>';
}

const showResult = () => {
    const x = document.getElementById("jumlahBil").value;
    const ratarata = document.getElementById("mean");
    const nilaiTerbesar = document.getElementById("biggest");
    var total = 0;
    max = 0;


    for (var id=1; id<=x; id++){
        var number = document.getElementById('angka'+id).value;
        total = total + parseInt(number);
        if (parseInt(number)>max){
            max = parseInt(number);
        }
    }
    console.log(total);
    meanNumber = total/x;

    ratarata.innerHTML = meanNumber;
    nilaiTerbesar.innerHTML = max;
}

function inputFaktorial() {
    const a = document.getElementById("angkaF").value;
    const ini = document.getElementById("faktorial");
    const result = faktorial(parseInt(a));

    ini.innerHTML = result;
}

function faktorial(n) {
    if (n == 1) {
        return 1;
    }
    else {
        return faktorial(n - 1) * n;
    }
}